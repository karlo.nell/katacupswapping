using System;
using System.Collections.Generic;
using System.Linq;

namespace KataCupSwapping
{
    public class Program
    {
        public static Dictionary<char, bool> Cups = new Dictionary<char, bool>()
        {
            { 'A', false },
            { 'B', false },
            { 'C', false },
        };

        public static void Main(string[] args)
        {
            Console.WriteLine(CupSwap("AC", "CA", "CA", "AC"));
        }
        public static string CupSwap(params string[] swaps)
        {
            //Reset needed for batch testing to keep dictionary data the same for each test
            Cups['A'] = false;
            //Ball always begins at position B
            Cups['B'] = true;
            Cups['C'] = false;

            //Check if tries to swap cup with itself return swap
            if (swaps.Any(c => c.EndsWith(c[0]))) return swaps[0];
            
            //Swap
            foreach (string swap in swaps)
            {
                bool holder = Cups[swap[0]];

                Cups[swap[0]] = Cups[swap[1]];
                Cups[swap[1]] = holder;


                Console.WriteLine($"swap {swap[0]} {Cups[swap[0]]} --> {swap[1]} to {Cups[swap[1]]}");
                foreach (var key in Cups)
                {
                    Console.WriteLine($"Key: {key.Key} Value: {key.Value}");
                }
                Console.WriteLine("------------------");
            }
            Console.WriteLine("Final ball position: ");
            
            //return where cup is at
            return Cups.Where(row => row.Value == true).Select(row => row.Key).FirstOrDefault().ToString();
        }
    }
}
